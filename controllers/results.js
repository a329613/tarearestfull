const express = require('express');

//method url action

//GET /users/  list
function list(req, res, next) {
    res.send('este es el get solito');
  };

//GET /users/{id}  index
function sumar(req, res, next) {
  //req.params nos permite acceder a los parametros de la url
  //req.boy nos permite acceder a los parametros de la peticion
  const n1 = parseInt(req.params.n1);
  const n2 = parseInt(req.params.n2);
  const resultado = n1 + n2;
  res.send(`Sumar ${n1} + ${n2} = ${resultado}`);

};

//POST /users/  create
function multiplicar(req, res, next) {
  const n1 = parseInt(req.body.n1);
  const n2= parseInt(req.body.n2);
  const resultado = n1*n2;
  res.send(`Multiplicar ${n1} * ${n2} = ${resultado}`);
};


//PUT /users/{id}  dividir
function dividir(req, res, next) {
  const n1 = parseInt(req.body.n1);
  const n2= parseInt(req.body.n2);
  const resultado = n1/n2;
  res.send(`Dividir ${n1} / ${n2} = ${resultado}`);
};

//PATCH /users/{id} potencia
function potencia(req, res, next) {
  const n1 = parseInt(req.body.n1);
  const n2= parseInt(req.body.n2);
  const resultado = Math.pow(n1,n2);
  res.send(`Potencia ${n1} ^ ${n2} = ${resultado}`);
};


//DELETE /users/{id} restar
function restar(req, res, next) {
  const n1 = parseInt(req.params.n1);
  const n2 = parseInt(req.params.n2);
  const resultado = n1 - n2;
  res.send(`Restar ${n1} - ${n2} = ${resultado}`);
};


module.exports = { list, sumar, multiplicar, dividir, potencia, restar };